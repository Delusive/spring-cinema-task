package ua.epam.spring.hometask.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class BaseAspect {
    @Pointcut("execution(public * ua.epam.spring.hometask.dao.*.*(..))")
    public void callAtDaoPublic() {
    }
}
