package ua.epam.spring.hometask.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class CacheDaoAspect extends BaseAspect {
    private static Logger logger = LogManager.getLogger(CacheDaoAspect.class);
    private Map<Object, Value> cache = new HashMap<>();

    private static final int CACHE_SECS = 5;

    @Around("callAtDaoPublic()")
    private Object checkCache(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String key = proceedingJoinPoint.toLongString() + Arrays.toString(proceedingJoinPoint.getArgs());
        var cachedValue = cache.get(key);
        if (cachedValue != null && cachedValue.getSaveTime() + CACHE_SECS * 1000 > System.currentTimeMillis()) {
            logger.debug("Returning cached value for " + proceedingJoinPoint.getSignature().toShortString());
            return cachedValue.getValue();
        }
        Value val = new Value(proceedingJoinPoint.proceed(), System.currentTimeMillis());
        cache.put(key, val);
        return val.getValue();
    }

    private static class Value {
        private Object value;
        private long saveTime;

        public Value(Object value, long saveTime) {
            this.value = value;
            this.saveTime = saveTime;
        }

        public Object getValue() {
            return value;
        }

        public long getSaveTime() {
            return saveTime;
        }
    }
}
