package ua.epam.spring.hometask.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspects.dao.CounterAspectDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@Aspect
public class CounterAspect {
    private static final Logger logger = LogManager.getLogger();
    private static long timeWhenTicketPriceQueryWasCountedLastTime = 0;

    private final CounterAspectDao counterAspectDao;

    @Autowired
    public CounterAspect(CounterAspectDao counterAspectDao) {
        this.counterAspectDao = counterAspectDao;
    }

    @Pointcut("execution(public * ua.epam.spring.hometask.service.EventService.getByName(java.lang.String))")
    private void eventServiceGetByName() {
    }

    @Pointcut("execution(public double ua.epam.spring.hometask.service.BookingService.getTicketsPrice(..))")
    private void bookingServiceGetTicketsPrice() {
    }

    @Pointcut("execution(public void ua.epam.spring.hometask.service.BookingService.bookTickets(..))")
    private void bookingServiceBookTickets() {
    }

    @AfterReturning(pointcut = "eventServiceGetByName()", returning = "returnedEvent")
    private void eventByNameAccessCount(Event returnedEvent) {
        if (returnedEvent != null) {
            counterAspectDao.registerNewEventQuery(returnedEvent);
        }
    }

    @After("bookingServiceGetTicketsPrice()")
    private void eventPricesQueryCount(JoinPoint joinPoint) {
        if (timeWhenTicketPriceQueryWasCountedLastTime + 5 * 1000 < System.currentTimeMillis()) { //caching
            Event targetEvent = (Event) joinPoint.getArgs()[0];
            counterAspectDao.registerEventPriceQuery(targetEvent);
            timeWhenTicketPriceQueryWasCountedLastTime = System.currentTimeMillis();
        }
    }

    @After("bookingServiceBookTickets()")
    private void eventTicketsBookedCount(JoinPoint joinPoint) {
        Set<Ticket> tickets = (Set<Ticket>) joinPoint.getArgs()[0];
        Set<Event> events = tickets.stream()
                .map(Ticket::getEvent)
                .collect(Collectors.toSet());
        events.forEach(counterAspectDao::registerEventTicketBooked);
    }

}
