package ua.epam.spring.hometask.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspects.dao.DiscountAspectDao;
import ua.epam.spring.hometask.domain.User;

@Component
@Aspect
public class DiscountAspect {
    private final DiscountAspectDao discountAspectDao;

    @Autowired
    public DiscountAspect(DiscountAspectDao discountAspectDao) {
        this.discountAspectDao = discountAspectDao;
    }

    @Pointcut("execution(public byte ua.epam.spring.hometask.service.DiscountService.getDiscountForTicket(..))")
    private void discountServiceGetDiscountForTicket() {}

    @AfterReturning(returning = "discount", pointcut = "discountServiceGetDiscountForTicket()")
    private void countOfDiscountApplying(JoinPoint joinPoint, byte discount) {
        if (discount != 0) {
            User targetUser = (User) joinPoint.getArgs()[0];
            discountAspectDao.registerDiscountApplying(targetUser);
        }
    }
}
