package ua.epam.spring.hometask.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerAspect extends BaseAspect {
    private static final Logger logger = LogManager.getLogger("aspectLogger");

    @Before("callAtDaoPublic()")
    private void someMethod(JoinPoint joinPoint) {
        StringBuilder logMessage = new StringBuilder("Executing ");
        logMessage.append(joinPoint.getSignature().toShortString());
        if (joinPoint.getArgs().length != 0) {
            logMessage.append(" with args ").append(arrayToString(joinPoint.getArgs()));
        }
        logger.debug(logMessage.toString());
    }

    private String arrayToString(Object[] array) {
        StringBuilder stringBuilder = new StringBuilder("[");
        for (Object obj : array) {
            if (obj instanceof String) {
                stringBuilder.append("\"").append(obj).append("\"");
            } else {
                stringBuilder.append(obj);
            }
            stringBuilder.append(", ");
        }
        int lastCommaIndex = stringBuilder.lastIndexOf(",");
        if (lastCommaIndex != -1) {
            stringBuilder.delete(lastCommaIndex, lastCommaIndex + 3);
        }
        return stringBuilder.append("]").toString();
    }
}
