package ua.epam.spring.hometask.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LuckyAspect {
    private static final byte CHANCE_TO_FREE_PURCHASE_IN_PERCENTS = 50;
    private static final Logger logger = LogManager.getLogger();

    @Pointcut("execution(public void ua.epam.spring.hometask.service.BookingService.bookTickets(..))")
    private void bookingServiceBookTicketsMethod() {
    }

    @Before("bookingServiceBookTicketsMethod()")
    private void checkLuck() {
        byte random = (byte) (Math.random() * 100 + 1);
        if (random <= CHANCE_TO_FREE_PURCHASE_IN_PERCENTS) { //user win
            logger.info("You are too lucky today! This purchase at the cinema expense! Congrats!");
        }
    }
}
