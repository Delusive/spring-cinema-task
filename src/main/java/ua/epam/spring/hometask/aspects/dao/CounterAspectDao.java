package ua.epam.spring.hometask.aspects.dao;

import ua.epam.spring.hometask.domain.Event;

public interface CounterAspectDao {
    /**
     * Invokes when user is querying event, needs to increment counter in database
     *
     * @param event event which counter should be incremented
     */
    void registerNewEventQuery(Event event);

    /**
     * Invokes when user is querying event prices, needs to increment counter in database
     *
     * @param event event which counter should be incremented
     */
    void registerEventPriceQuery(Event event);

    /**
     * Invokes when user is booking event ticket, needs to increment counter in database
     *
     * @param event event which counter should be incremented
     */
    void registerEventTicketBooked(Event event);
}
