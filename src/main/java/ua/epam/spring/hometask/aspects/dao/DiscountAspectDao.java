package ua.epam.spring.hometask.aspects.dao;

import ua.epam.spring.hometask.domain.User;

public interface DiscountAspectDao {
    /**
     * Invokes when discount is applying to the purchase, needs to increment counter in database
     *
     * @param user user for whom discount was applied
     */
    void registerDiscountApplying(User user);
}
