package ua.epam.spring.hometask.aspects.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspects.dao.CounterAspectDao;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.domain.Event;

import java.util.List;

@Component
public class CounterAspectDaoImpl implements CounterAspectDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CounterAspectDaoImpl(DatabaseManager databaseManager) {
        jdbcTemplate = databaseManager.getJdbcTemplate();
    }

    @Override
    public void registerNewEventQuery(Event event) {
        registerAction(event, EventAction.QUERY_EVENT_BY_NAME);
    }

    @Override
    public void registerEventPriceQuery(Event event) {
        registerAction(event, EventAction.QUERY_EVENT_PRICES);
    }

    @Override
    public void registerEventTicketBooked(Event event) {
        registerAction(event, EventAction.TICKETS_WAS_BOOKED);
    }

    private void registerAction(Event event, EventAction eventAction) {
        String sql = "SELECT event_counters.id FROM event_counters JOIN counter_types ON (event_counters.type = counter_types.id) WHERE counter_types.name = ? AND event_counters.event_id = ?";
        List<Integer> statIds = jdbcTemplate.query(sql, new Object[]{eventAction.toString(), event.getId()}, (resultSet, i) -> resultSet.getInt("event_counters.id"));
        sql = statIds.isEmpty() ?
                "INSERT INTO event_counters (`event_id`, `type`, `count`) VALUES (?, (SELECT id FROM counter_types WHERE counter_types.name = ?), 1)" :
                "UPDATE event_counters SET event_counters.`count` = event_counters.`count` + 1 WHERE event_counters.event_id = ? AND event_counters.type = (SELECT counter_types.id FROM counter_types WHERE counter_types.name = ?)";
        jdbcTemplate.update(sql, event.getId(), eventAction.toString());
    }

    private enum EventAction {QUERY_EVENT_BY_NAME, QUERY_EVENT_PRICES, TICKETS_WAS_BOOKED}
}
