package ua.epam.spring.hometask.aspects.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspects.dao.DiscountAspectDao;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.domain.User;

import java.util.List;

@Component
public class DiscountAspectDaoImpl implements DiscountAspectDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DiscountAspectDaoImpl(DatabaseManager databaseManager) {
        this.jdbcTemplate = databaseManager.getJdbcTemplate();
    }

    @Override
    public void registerDiscountApplying(User user) {
        String sql = "SELECT id FROM discount_counter WHERE user_id = ?";
        List<Integer> ids = jdbcTemplate.query(sql, new Object[]{user.getId()}, (resultSet, i) -> resultSet.getInt("id"));
        sql = ids.isEmpty() ?
                "INSERT INTO discount_counter (user_id, count) VALUES (?, 1)" :
                "UPDATE discount_counter SET `count` = `count` + 1 WHERE user_id = ?";
        jdbcTemplate.update(sql, user.getId());
    }
}
