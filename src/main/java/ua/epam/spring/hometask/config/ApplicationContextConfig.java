package ua.epam.spring.hometask.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("ua.epam.spring.hometask")
@EnableAspectJAutoProxy
public class ApplicationContextConfig {
}
