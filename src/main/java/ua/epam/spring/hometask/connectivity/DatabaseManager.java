package ua.epam.spring.hometask.connectivity;

import org.springframework.jdbc.core.JdbcTemplate;

public interface DatabaseManager {
    JdbcTemplate getJdbcTemplate();
}
