package ua.epam.spring.hometask.connectivity.impl;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.connectivity.DatabaseManager;

@Component
@PropertySource("classpath:cinema.properties")
public class DatabaseManagerImpl implements DatabaseManager {
    private final JdbcTemplate jdbcTemplate;

    public DatabaseManagerImpl(@Value("${db.class.name}") String driverClassName,
                               @Value("${db.url}") String url,
                               @Value("${db.username}") String username,
                               @Value("${db.password}") String password) {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource(url, username, password);
        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setURL(url);
        mysqlDataSource.setUser(username);
        mysqlDataSource.setPassword(password);
        //dataSource.setDriverClassName(driverClassName);
        jdbcTemplate = new JdbcTemplate(mysqlDataSource);
    }

    @Override
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
