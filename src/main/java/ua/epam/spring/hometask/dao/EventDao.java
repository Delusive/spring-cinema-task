package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Event;

import java.util.List;

public interface EventDao {
    List<Event> getEvents();

    Event getEventByName(String name);

    Event getEventById(int id);

    void delete(Event event);

    void save(Event event);

    void updateSessions(Event event);
}
