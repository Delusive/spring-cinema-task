package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface TicketDao {
    List<Ticket> getTickets();

    List<Ticket> getUserTickets(User user);

    Set<Ticket> getPurchasedTicketsForEvent( Event event,  LocalDateTime dateTime);

    void save(Ticket ticket);
}
