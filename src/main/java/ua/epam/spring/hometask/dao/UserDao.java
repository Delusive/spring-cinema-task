package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.User;

import java.util.List;

public interface UserDao {
    List<User> getUsers();

    User getByEmail(String email);

    User getById(long id);

    void save(User user);

    void delete(User user);
}
