package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.dao.AuditoriumDao;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.HashSet;
import java.util.Set;

@Component
public class AuditoriumDaoImpl implements AuditoriumDao {
    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<Auditorium> auditoriumMapper;

    @Autowired
    public AuditoriumDaoImpl(DatabaseManager databaseManager) {
        jdbcTemplate = databaseManager.getJdbcTemplate();

        auditoriumMapper = (resultSet, i) -> {
            var auditorium = new Auditorium();
            auditorium.setName(resultSet.getString("name"));
            auditorium.setNumberOfSeats(resultSet.getInt("number_of_seats"));
            auditorium.setVipSeats(getVipSeatsForAuditorium(resultSet.getInt("id")));
            return auditorium;
        };
    }

    @Override
    public Set<Auditorium> getAuditoriums() {
        String sql = "SELECT id, name, number_of_seats FROM auditoriums";
        return new HashSet<>(jdbcTemplate.query(sql, auditoriumMapper));
    }

    @Override
    public Auditorium getByName(String name) {
        String sql = "SELECT id, name, number_of_seats FROM auditoriums WHERE name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, auditoriumMapper)
                .stream().findAny().orElse(null);
    }

    @Override
    public void save(Auditorium auditorium) {
        String sql = "INSERT INTO auditoriums (name, number_of_seats) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, auditorium.getName(), auditorium.getNumberOfSeats());
    }

    private Set<Long> getVipSeatsForAuditorium(int id) {
        String sql = "SELECT id, seat FROM vip_seats WHERE auditorium_id = ?";
        RowMapper<Long> rowMapper = (resultSet, i) -> resultSet.getLong("seat");
        return new HashSet<>(jdbcTemplate.query(sql, new Object[]{id}, rowMapper));
    }
}
