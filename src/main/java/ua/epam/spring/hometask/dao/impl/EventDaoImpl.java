package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.dao.AuditoriumDao;
import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

@Component
public class EventDaoImpl implements EventDao {
    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<Event> eventRowMapper;
    private final AuditoriumDao auditoriumDao;

    @Autowired
    public EventDaoImpl(DatabaseManager databaseManager, AuditoriumDao auditoriumDao) {
        jdbcTemplate = databaseManager.getJdbcTemplate();
        this.auditoriumDao = auditoriumDao;

        eventRowMapper = (resultSet, i) -> {
            Event event = new Event();
            int eventId = resultSet.getInt("id");
            event.setId(eventId);
            event.setName(resultSet.getString("name"));
            event.setBasePrice(resultSet.getDouble("base_price"));
            event.setRating(EventRating.valueOf(resultSet.getString("event_ratings.name")));
            event.setAuditoriums(getSessionsForEvent(eventId));
            return event;
        };
    }

    @Override
    public List<Event> getEvents() {
        String sql = "SELECT events.id, events.name, base_price, event_ratings.name FROM events JOIN event_ratings ON events.rating_id = event_ratings.id";
        return jdbcTemplate.query(sql, eventRowMapper);
    }

    @Override
    public Event getEventByName(String name) {
        String sql = "SELECT events.id, events.name, base_price, event_ratings.name FROM events JOIN event_ratings ON events.rating_id = event_ratings.id WHERE events.name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, eventRowMapper)
                .stream().findAny().orElse(null);
    }

    @Override
    public Event getEventById(int id) {
        String sql = "SELECT events.id, events.name, base_price, event_ratings.name FROM events JOIN event_ratings ON events.rating_id = event_ratings.id WHERE events.id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, eventRowMapper)
                .stream().findAny().orElse(null);
    }

    @Override
    public void delete(Event event) {
        String sql = "UPDATE events SET is_active = 0 WHERE id = ?";
        jdbcTemplate.update(sql, event.getId());
    }

    @Override
    public void save(Event event) {
        String sql = "INSERT INTO events (name, base_price, rating_id) VALUES (?, ?, (SELECT event_ratings.id FROM event_ratings WHERE event_ratings.name = ?))";
        jdbcTemplate.update(sql, event.getName(), event.getBasePrice(), event.getRating().toString());
    }

    @Override
    public void updateSessions(Event event) {
        String deleteSql = "DELETE FROM sessions WHERE event_id = ?";
        jdbcTemplate.update(deleteSql, event.getId());

        String insertSql = "INSERT INTO sessions (event_id, auditorium_id, time) VALUES (?, (SELECT auditoriums.id FROM auditoriums WHERE name = ?), ?)";
        event.getAuditoriums().forEach((key, value) -> {
            int eventId = event.getId();
            String auditoriumName = value.getName();
            Timestamp timestamp = Timestamp.valueOf(key);
            jdbcTemplate.update(insertSql, eventId, auditoriumName, timestamp);
        });
    }

    private NavigableMap<LocalDateTime, Auditorium> getSessionsForEvent(int eventId) {
        String sql = "SELECT event_id, auditoriums.name, time FROM sessions JOIN auditoriums ON sessions.auditorium_id = auditoriums.id WHERE event_id = ?";
        return jdbcTemplate.query(sql, new Object[]{eventId}, (resultSet) -> {
            NavigableMap<LocalDateTime, Auditorium> response = new TreeMap<>();
            while (resultSet.next()) {
                LocalDateTime dateTime = resultSet.getTimestamp("time").toLocalDateTime();
                Auditorium auditorium = auditoriumDao.getByName(resultSet.getString("auditoriums.name"));
                response.put(dateTime, auditorium);
            }
            return response;
        });
    }
}
