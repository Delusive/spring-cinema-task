package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.dao.TicketDao;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class TicketDaoImpl implements TicketDao {
    @Autowired
    private ApplicationContext context;
    private final JdbcTemplate jdbcTemplate;
    private RowMapper<Ticket> ticketRowMapper;


    @Autowired
    public TicketDaoImpl(DatabaseManager databaseManager) {
        jdbcTemplate = databaseManager.getJdbcTemplate();
    }

    @PostConstruct
    private void init() {
        ticketRowMapper = (resultSet, i) -> {
            int id = resultSet.getInt("id");
            User user = context.getBean(UserDao.class).getById(resultSet.getInt("user_id"));
            Event event = context.getBean(EventDao.class).getEventById(resultSet.getInt("event_id"));
            LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();
            long seat = resultSet.getInt("seat");
            return new Ticket(id, user, event, time, seat);
        };
    }

    @Override
    public List<Ticket> getTickets() {
        String sql = "SELECT id, user_id, event_id, time, seat FROM tickets";
        return jdbcTemplate.query(sql, ticketRowMapper);
    }

    @Override
    public List<Ticket> getUserTickets(User user) {
        String sql = "SELECT id, event_id, time, seat FROM tickets WHERE user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{user.getId()}, (resultSet, i) -> {
            int id = resultSet.getInt("id");
            Event event = context.getBean(EventDao.class).getEventById(resultSet.getInt("event_id"));
            LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();
            long seat = resultSet.getLong("seat");
            return new Ticket(id, user, event, time, seat);
        });
    }

    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
        String sql = "SELECT id, user_id, event_id, time, seat FROM tickets WHERE event_id = ? AND time = ?";
        return new HashSet<>(jdbcTemplate.query(sql, new Object[]{event.getId(), Timestamp.valueOf(dateTime)}, ticketRowMapper));
    }

    @Override
    public void save(Ticket ticket) {
        String sql = "INSERT INTO tickets (user_id, event_id, time, seat) VALUES (?, ?, ?, ?)";
        int userId = ticket.getUser() == null ? -1 : ticket.getUser().getId();
        jdbcTemplate.update(sql, userId, ticket.getEvent().getId(), Timestamp.valueOf(ticket.getDateTime()), ticket.getSeat());
    }

}
