package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.connectivity.DatabaseManager;
import ua.epam.spring.hometask.dao.TicketDao;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.domain.UserRole;

import java.sql.Date;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Component
public class UserDaoImpl implements UserDao {
    private final JdbcTemplate jdbcTemplate;
    private final RowMapper<User> userRowMapper;
    private final TicketDao ticketDao;

    @Autowired
    public UserDaoImpl(DatabaseManager databaseManager, TicketDao ticketDao) {
        jdbcTemplate = databaseManager.getJdbcTemplate();
        this.ticketDao = ticketDao;

        userRowMapper = (resultSet, i) -> {
            User user = new User();
            int userId = resultSet.getInt("id");
            user.setId(userId);
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setBirthday(resultSet.getDate("birthday").toLocalDate());
            user.setRole(UserRole.valueOf(resultSet.getString("roles.name")));
            user.setTickets(getTicketsForUser(user));
            return user;
        };
    }

    @Override
    public List<User> getUsers() {
        String sql = "SELECT users.id, email, password, first_name, last_name, birthday, roles.name FROM users JOIN roles ON roles.id = users.role_id";
        return jdbcTemplate.query(sql, userRowMapper);
    }

    @Override
    public User getByEmail(String email) {
        String sql = "SELECT users.id, email, password, first_name, last_name, birthday, roles.name FROM users JOIN roles ON roles.id = users.role_id WHERE email = ?";
        return jdbcTemplate.query(sql, new Object[]{email}, userRowMapper)
                .stream().findAny().orElse(null);
    }

    @Override
    public User getById(long id) {
        String sql = "SELECT users.id, email, password, first_name, last_name, birthday, roles.name FROM users JOIN roles ON roles.id = users.role_id WHERE users.id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, userRowMapper)
                .stream().findAny().orElse(null);
    }

    @Override
    public void save(User user) {
        String sql = "INSERT INTO users (email, password, first_name, last_name, birthday, role_id) VALUES (?, ?, ?, ?, ?, (SELECT roles.id FROM roles WHERE roles.name = ?))";
        jdbcTemplate.update(sql, user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), Date.valueOf(user.getBirthday()), user.getRole().toString());
    }

    @Override
    public void delete(User user) {
        String sql = "DELETE FROM users WHERE id = ?";
        jdbcTemplate.update(sql, user.getId());
    }

    private NavigableSet<Ticket> getTicketsForUser(User user) {
        return new TreeSet<>(ticketDao.getUserTickets(user));
    }
}
