package ua.epam.spring.hometask.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Yuriy_Tkach
 */
public class Event extends DomainObject {
    private String name;
    private double basePrice;
    private EventRating rating;
    private NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();

    /**
     * Removes auditorium assignment from event
     *
     * @param dateTime Date and time to remove auditorium for
     * @return <code>true</code> if successful, <code>false</code> if not
     * removed
     */
    public boolean removeAuditoriumAssignment(LocalDateTime dateTime) {
        return auditoriums.remove(dateTime) != null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public EventRating getRating() {
        return rating;
    }

    public void setRating(EventRating rating) {
        this.rating = rating;
    }

    public NavigableMap<LocalDateTime, Auditorium> getAuditoriums() {
        return auditoriums;
    }

    public void setAuditoriums(NavigableMap<LocalDateTime, Auditorium> auditoriums) {
        this.auditoriums = auditoriums;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Event other = (Event) obj;
        if (name == null) {
            return other.name == null;
        } else return name.equals(other.name);
    }

    @Override
    public String toString() {
        StringBuilder response = new StringBuilder();
        response.append("\tTitle: \"").append(name).append("\"\n")
                .append("\tRating: ").append(rating).append("\n")
                .append("\tBase price: ").append(basePrice).append(" (vip x2)\n")
                .append("\tTimings: ");
        if (auditoriums.isEmpty()) response.append("no timings found");
        auditoriums.forEach((localDateTime, auditorium) -> {
            response.append("\n")
                    .append("\t\tTime: ")
                    .append(localDateTime.format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                    .append(" | Auditorium: ")
                    .append(auditorium.getName());
        });
        return response.toString();
    }
}
