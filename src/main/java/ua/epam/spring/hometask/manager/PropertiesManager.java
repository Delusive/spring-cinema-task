package ua.epam.spring.hometask.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.manager.exception.PropertiesManagerException;

import java.io.IOException;
import java.util.Properties;

@Component("messagesProps")
public class PropertiesManager {
    private final Properties props = new Properties();

    public PropertiesManager(@Value("/messages_ru.properties") String pathToFile) {
        var inputStream = getClass().getResourceAsStream(pathToFile);
        try {
            props.load(inputStream);
        } catch (IOException e) {
            throw new PropertiesManagerException(e);
        }
    }

    public String getString(String param) {
        if (param == null) throw new NullPointerException("Argument must be not null");
        return props.getProperty(param);
    }
}
