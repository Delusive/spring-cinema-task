package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

/**
 * @author Yuriy_Tkach
 */
public interface DiscountService {

    /**
     * Getting discount based on some rules for user that buys some number of
     * tickets for the specific date time of the event
     *
     * @param user         User that buys tickets. Can be <code>null</code>
     * @param event        Event that tickets are bought for
     * @param airDateTime  The date and time event will be aired
     * @param ticketNumber Number of ticket in one purchase
     * @return discount value from 0 to 100
     */
    byte getDiscountForTicket( User user,  Event event,  LocalDateTime airDateTime, long ticketNumber);

}
