package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public interface DiscountStrategy {
    /**
     * Get discount between 0 and 100 percents
     *
     * @param user         User for which discount should be calculated
     * @param event        Event for which discount should be calculated
     * @param airDateTime  <noscript>paste smth here</noscript>
     * @param ticketNumber Ticket number in one purchase
     * @return Number between 0 and 100 that means discount in percents to that ticket
     */
    byte calculateDiscountForTicket( User user,  Event event,  LocalDateTime airDateTime, long ticketNumber);
}
