package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.AuditoriumDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.service.AuditoriumService;

import java.util.Set;

@Component
public class AuditoriumServiceImpl implements AuditoriumService {
    private final AuditoriumDao auditoriumDao;

    @Autowired
    public AuditoriumServiceImpl(AuditoriumDao auditoriumDao) {
        this.auditoriumDao = auditoriumDao;
    }


    @Override
    public Set<Auditorium> getAll() {
        return auditoriumDao.getAuditoriums();
    }


    @Override
    public Auditorium getByName( String name) {
        return auditoriumDao.getByName(name);
    }
}
