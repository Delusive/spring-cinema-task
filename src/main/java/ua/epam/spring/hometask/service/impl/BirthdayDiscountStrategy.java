package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountStrategy;

import java.time.LocalDateTime;

@Component
public class BirthdayDiscountStrategy implements DiscountStrategy {
    private final byte DISCOUNT;

    public BirthdayDiscountStrategy(@Value("5") byte discount) {
        DISCOUNT = discount;
    }

    @Override
    public byte calculateDiscountForTicket(User user, Event event, LocalDateTime airDateTime, long ticketNumber) {
        if (user == null) return 0;
        var beginOfTheBirthday = user.getBirthday().atStartOfDay();
        boolean isPlusFiveDaysAfterBirthday = airDateTime.plusDays(5).isAfter(beginOfTheBirthday);
        boolean isMinusFiveDaysBeforeBirthday = airDateTime.minusDays(5).isBefore(beginOfTheBirthday);

        return isPlusFiveDaysAfterBirthday && isMinusFiveDaysBeforeBirthday ? DISCOUNT : 0;
    }
}
