package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.TicketDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookingServiceImpl implements BookingService {
    private DiscountService discountService;
    private TicketDao ticketDao;

    @Autowired
    public BookingServiceImpl(DiscountService discountService, TicketDao ticketDao) {
        this.discountService = discountService;
        this.ticketDao = ticketDao;
    }

    @Override
    public double getTicketsPrice( Event event,  LocalDateTime dateTime, User user,  Set<Long> seats) {
        double totalPrice = 0;
        int ticketNumber = 1;
        for (var seatIterator = seats.iterator(); seatIterator.hasNext(); ticketNumber++) {
            var seat = seatIterator.next();
            double ticketPrice = event.getBasePrice();
            var auditorium = event.getAuditoriums().get(dateTime);
            if (auditorium.getVipSeats().contains(seat)) ticketPrice *= 2;
            if (event.getRating() == EventRating.HIGH) ticketPrice *= 1.2D;
            ticketPrice *= (100 - discountService.getDiscountForTicket(user, event, dateTime, ticketNumber)) / 100D;
            totalPrice += ticketPrice;
        }
        return totalPrice;
    }

    @Override
    public void bookTickets( Set<Ticket> tickets) {
        tickets.forEach(ticketDao::save);
    }


    @Override
    public Set<Ticket> getPurchasedTicketsForEvent( Event event,  LocalDateTime dateTime) {
        return ticketDao.getPurchasedTicketsForEvent(event, dateTime);
    }

    @Override
    public Set<Long> getAvailableSeatsForEvent(Event event, LocalDateTime dateTime) {
        Set<Long> availableSeats = new HashSet<>();
        Set<Long> unavailableSeats = getPurchasedTicketsForEvent(event, dateTime).stream()
                .mapToLong(Ticket::getSeat)
                .collect(HashSet::new, HashSet::add, HashSet::addAll);
        var seats = event.getAuditoriums().get(dateTime).getNumberOfSeats();
        for (long i = 1; i <= seats; i++) {
            if (!unavailableSeats.contains(i)) availableSeats.add(i);
        }
        return availableSeats;
    }
}
