package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.DiscountStrategy;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class DiscountServiceImpl implements DiscountService {
    private final List<DiscountStrategy> discountStrategies;

    @Autowired
    public DiscountServiceImpl(List<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }

    @Override
    public byte getDiscountForTicket( User user,  Event event,  LocalDateTime airDateTime, long ticketNumber) {
        byte maxDiscount = 0;
        for (DiscountStrategy strategy : discountStrategies) {
            byte discount = strategy.calculateDiscountForTicket(user, event, airDateTime, ticketNumber);
            if (discount > maxDiscount) maxDiscount = discount;
        }
        return maxDiscount;
    }

}
