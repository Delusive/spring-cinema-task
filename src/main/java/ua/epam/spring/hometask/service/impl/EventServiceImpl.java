package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.NavigableMap;

@Component
public class EventServiceImpl implements EventService {
    private final EventDao eventDao;

    @Autowired
    public EventServiceImpl(EventDao eventDao) {
        this.eventDao = eventDao;
    }


    @Override
    public Event getByName(String name) {
        return eventDao.getEventByName(name);
    }

    @Override
    public void applyAuditoriumToEvent(Event event, Auditorium auditorium, LocalDateTime dateTime) {
        NavigableMap<LocalDateTime, Auditorium> auditoriums = event.getAuditoriums();
        auditoriums.put(dateTime, auditorium);
        eventDao.updateSessions(event);
    }

    @Override
    public Event save(Event event) {
        eventDao.save(event);
        return eventDao.getEventById(event.getId());
    }

    @Override
    public void remove(Event event) {
        eventDao.delete(event);
    }

    @Override
    public Event getById(int id) {
        return eventDao.getEventById(id);
    }


    @Override
    public Collection<Event> getAll() {
        return eventDao.getEvents();
    }
}
