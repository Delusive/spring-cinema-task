package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountStrategy;

import java.time.LocalDateTime;

@Component
public class TenTicketsDiscountStrategy implements DiscountStrategy {
    private final byte DISCOUNT;

    public TenTicketsDiscountStrategy(@Value("50") byte discount) {
        DISCOUNT = discount;
    }

    @Override
    public byte calculateDiscountForTicket(User user, Event event, LocalDateTime airDateTime, long ticketNumber) {
        if (user == null) {
            return ticketNumber % 10 == 0 ? DISCOUNT : 0;
        }
        return (user.getTickets().size() + ticketNumber) % 10 == 0 ? DISCOUNT : 0;
    }
}
