package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

import java.util.Collection;

@Component
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }


    @Override
    public User getUserByEmail(String email) {
        return userDao.getByEmail(email);
    }

    @Override
    public User save(User user) {
        userDao.save(user);
        return userDao.getById(user.getId());
    }

    @Override
    public void remove(User user) {
        userDao.delete(user);
    }

    @Override
    public User getById(int id) {
        return userDao.getById(id);
    }


    @Override
    public Collection<User> getAll() {
        return userDao.getUsers();
    }
}
