package ua.epam.spring.hometask.ui.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.*;
import ua.epam.spring.hometask.manager.PropertiesManager;
import ua.epam.spring.hometask.service.AuditoriumService;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;
import ua.epam.spring.hometask.ui.UserInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Component
public class ConsoleUserInterface implements UserInterface {
    private static final byte LOGIN_OR_NOT = 0;
    private static final byte LOGIN_MENU_ENTERING_LOGIN = 1;
    private static final byte LOGIN_MENU_ENTERING_PASSWORD = 2;
    private static final byte REGISTER_ENTERING_EMAIL = 3;
    private static final byte REGISTER_ENTERING_FIRST_NAME = 4;
    private static final byte REGISTER_ENTERING_LAST_NAME = 5;
    private static final byte REGISTER_ENTERING_BIRTHDAY = 6;
    private static final byte REGISTER_ENTERING_PASSWORD = 7;
    private static final byte MAIN_MENU_USER = 8;
    private static final byte CHOOSING_EVENT = 9;
    private static final byte CHOOSING_EVENT_TIME = 10;
    private static final byte CHOOSING_SEATS = 11;
    private static final byte CONFIRMATION_TICKET_PURCHASE = 12;
    private static final byte PRESS_ENTER_TO_SHOW_MAIN_MENU = 13;
    private static final byte MAIN_MENU_ADMIN = 14;
    private static final byte ADD_EVENT_ENTERING_EVENT_NAME = 15;
    private static final byte ADD_EVENT_ENTERING_PRICE = 16;
    private static final byte ADD_EVENT_ENTERING_RATING = 17;
    private static final byte APPLY_AUDITORIUM_CHOOSING_EVENT = 18;
    private static final byte APPLY_AUDITORIUM_ENTERING_DATE = 19;
    private static final byte APPLY_AUDITORIUM_CHOOSING_AUDITORIUM = 20;
    private static final byte VIEWING_PURCHASED_TICKETS_CHOOSING_EVENT = 21;
    private static final byte VIEWING_PURCHASED_TICKETS_CHOOSING_TIME = 22;

    private static final Logger logger = LogManager.getLogger();
    private final PropertiesManager messages;
    private final UserService userService;
    private final EventService eventService;
    private final BookingService bookingService;
    private final AuditoriumService auditoriumService;


    private int currentStage = 0;
    // temp information
    private String email;
    private User user;
    private Event event;
    private LocalDateTime localDateTime;
    private Set<Long> seats = new HashSet<>();

    @Autowired
    public ConsoleUserInterface(@Qualifier("messagesProps") PropertiesManager messages, UserService userService, EventService eventService, BookingService bookingService, AuditoriumService auditoriumService) {
        this.messages = messages;
        this.userService = userService;
        this.eventService = eventService;
        this.bookingService = bookingService;
        this.auditoriumService = auditoriumService;
    }

    @Override
    public void delegate() {
        Scanner in = new Scanner(System.in);
        displayWelcomeMessage();
        while (true) {
            String inputLine = in.nextLine();
            handleUserInput(inputLine);
        }
    }

    private void displayWelcomeMessage() {
        logger.info(messages.getString("menu.welcome"));
    }


    // main menu:

    private void handleUserInput(String inputLine) {
        String response = "";
        try {
            switch (currentStage) {
                case LOGIN_OR_NOT:
                    response = messageOnLoginOrNotMenu(inputLine);
                    break;
                case LOGIN_MENU_ENTERING_LOGIN:
                case LOGIN_MENU_ENTERING_PASSWORD:
                    response = messageOnLoginMenu(inputLine);
                    break;
                case REGISTER_ENTERING_EMAIL:
                case REGISTER_ENTERING_FIRST_NAME:
                case REGISTER_ENTERING_LAST_NAME:
                case REGISTER_ENTERING_BIRTHDAY:
                case REGISTER_ENTERING_PASSWORD:
                    response = messageOnRegister(inputLine);
                    break;
                case MAIN_MENU_USER:
                    response = messageOnMainMenuUser(inputLine);
                    break;
                case CHOOSING_EVENT:
                case CHOOSING_EVENT_TIME:
                case CHOOSING_SEATS:
                case CONFIRMATION_TICKET_PURCHASE:
                    response = messageOnChoosingEventMenu(inputLine);
                    break;
                case PRESS_ENTER_TO_SHOW_MAIN_MENU:
                    response = messageOnPressEnterToShowMainMenu();
                    break;
                case MAIN_MENU_ADMIN:
                    response = messageOnMainMenuAdmin(inputLine);
                    break;
                case ADD_EVENT_ENTERING_EVENT_NAME:
                case ADD_EVENT_ENTERING_PRICE:
                case ADD_EVENT_ENTERING_RATING:
                    response = messageWhileAddingNewEvent(inputLine);
                    break;
                case APPLY_AUDITORIUM_CHOOSING_EVENT:
                case APPLY_AUDITORIUM_ENTERING_DATE:
                case APPLY_AUDITORIUM_CHOOSING_AUDITORIUM:
                    response = messageWhileApplyingAuditoriumToTheEvent(inputLine);
                    break;
                case VIEWING_PURCHASED_TICKETS_CHOOSING_EVENT:
                case VIEWING_PURCHASED_TICKETS_CHOOSING_TIME:
                    response = messageWhileViewingBoughtTickets(inputLine);
                    break;

            }
        } catch (NumberFormatException e) {
            response = messages.getString("not.an.integer");
            logger.debug(e);
        } catch (IndexOutOfBoundsException e) {
            response = messages.getString("invalid.choice.from.list");
            logger.debug(e);
        }
        logger.info(response);
    }

    private String messageOnMainMenuUser(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        switch (choice) {
            case 1:
                return messageOnChoseViewEvents();
            case 2:
                return messageOnLogout();
            default:
                return messages.getString("menu.main.invalid.choice");
        }
    }

    private String messageOnChoseViewEvents() {
        currentStage = CHOOSING_EVENT;
        var events = eventService.getAll();
        return events.isEmpty() ? messages.getString("view.events.not.found") : messages.getString("view.events.header") + stringifyEventList(events) + messages.getString("view.events.footer");
    }

    // :main menu end


    // Login:

    private String messageOnLogout() {
        currentStage = LOGIN_OR_NOT;
        user = null;
        email = null;
        return messages.getString("menu.welcome");
    }

    private String messageOnLoginOrNotMenu(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        if (choice == 1) {
            currentStage = LOGIN_MENU_ENTERING_LOGIN;
            return messages.getString("signing.in.enter.email");
        } else if (choice == 2) {
            currentStage = MAIN_MENU_USER;
            return messages.getString("menu.main.user");
        } else if (choice == 3) {
            currentStage = REGISTER_ENTERING_EMAIL;
            return messages.getString("register.enter.email");
        }
        return messages.getString("login.or.not.invalid.choice");
    }

    // :Login end


    // Register:

    private String messageOnLoginMenu(String inputLine) {
        if (currentStage == LOGIN_MENU_ENTERING_LOGIN) {
            email = inputLine;
            currentStage = LOGIN_MENU_ENTERING_PASSWORD;
            return messages.getString("signing.in.enter.password");
        } else if (currentStage == LOGIN_MENU_ENTERING_PASSWORD) {
            User user = userService.getUserByEmail(email);
            if (user != null && user.getPassword().equals(inputLine)) {
                this.user = user;
                if (user.getRole() == UserRole.ADMIN) {
                    currentStage = MAIN_MENU_ADMIN;
                    return messages.getString("menu.main.admin");
                } else {
                    currentStage = MAIN_MENU_USER;
                    return messages.getString("menu.main.user");
                }
            } else {
                return messages.getString("signing.in.invalid.password");
            }
        }
        return null;
    }

    private String messageOnRegister(String inputLine) {
        switch (currentStage) {
            case REGISTER_ENTERING_EMAIL:
                return messageOnEnteringEmailWhileRegistering(inputLine);
            case REGISTER_ENTERING_FIRST_NAME:
                return messageOnEnteringFirstNameWhileRegistering(inputLine);
            case REGISTER_ENTERING_LAST_NAME:
                return messageOnEnteringLastNameWhileRegistering(inputLine);
            case REGISTER_ENTERING_BIRTHDAY:
                return messageOnEnteringBirthdayWhileRegistering(inputLine);
            case REGISTER_ENTERING_PASSWORD:
                return messageOnEnteringPasswordWhileRegistering(inputLine);
        }
        return null;
    }

    private String messageOnEnteringEmailWhileRegistering(String inputLine) {
        String emailPattern = ".+@.+\\..+";
        if (!inputLine.matches(emailPattern)) return messages.getString("register.email.invalid");
        user = new User();
        user.setEmail(inputLine.trim());
        currentStage = REGISTER_ENTERING_FIRST_NAME;
        return messages.getString("register.enter.first.name");
    }

    private String messageOnEnteringFirstNameWhileRegistering(String inputLine) {
        user.setFirstName(inputLine.trim());
        currentStage = REGISTER_ENTERING_LAST_NAME;
        return messages.getString("register.enter.last.name");
    }

    private String messageOnEnteringLastNameWhileRegistering(String inputLine) {
        user.setLastName(inputLine.trim());
        currentStage = REGISTER_ENTERING_BIRTHDAY;
        return messages.getString("register.enter.birthday");
    }

    private String messageOnEnteringBirthdayWhileRegistering(String inputLine) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD-MM-YYYY");
        try {
            LocalDate localDate = simpleDateFormat.parse(inputLine)
                    .toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            user.setBirthday(localDate);
            currentStage = REGISTER_ENTERING_PASSWORD;
            return messages.getString("register.enter.password");
        } catch (ParseException e) {
            logger.debug(e);
            return messages.getString("register.birthday.invalid");
        }
    }

    // :register end


    // choosing event:

    private String messageOnEnteringPasswordWhileRegistering(String inputLine) {
        user.setPassword(inputLine);
        userService.save(user);
        currentStage = MAIN_MENU_USER;
        return messages.getString("menu.main.user");
    }

    private String messageOnChoosingEventMenu(String inputLine) {
        if (inputLine.equals("0")) {
            currentStage = MAIN_MENU_USER;
            return messages.getString("menu.main.user");
        }
        switch (currentStage) {
            case CHOOSING_EVENT:
                return messageOnChoosingEvent(inputLine);
            case CHOOSING_EVENT_TIME:
                return messageOnChoosingEventTime(inputLine);
            case CHOOSING_SEATS:
                return messageOnChoosingSeats(inputLine);
            case CONFIRMATION_TICKET_PURCHASE:
                return messageOnConfirmPurchase(inputLine);
        }
        return null;
    }

    private String messageOnChoosingEvent(String inputLine) {
        int choseEventNumber = Integer.parseInt(inputLine);
        var eventList = new ArrayList<>(eventService.getAll());
        //Event event = eventList.get(choseEventNumber - 1);
        String eventName = eventList.get(choseEventNumber - 1).getName();
        Event event = eventService.getByName(eventName); // я не умственно-отсталый, правда, это нужно чтобы сработал аспект
        if (event.getAuditoriums().isEmpty()) {
            return messages.getString("view.events.no.auditoriums.available");
        }
        StringBuilder response = new StringBuilder(messages.getString("view.events.choose.time"));
        int i = 1;
        for (Map.Entry<LocalDateTime, Auditorium> entry : event.getAuditoriums().entrySet()) {
            LocalDateTime localDateTime = entry.getKey();
            Auditorium auditorium = entry.getValue();
            long availableSeats = auditorium.getNumberOfSeats() - bookingService.getPurchasedTicketsForEvent(event, localDateTime).size();
            response.append('\n')
                    .append(i).append(". ")
                    .append("Date: ")
                    .append(localDateTime.format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                    .append(", Auditorium: ")
                    .append(auditorium.getName())
                    .append(", ").append(availableSeats).append(" seats available");
            i++;
        }
        this.event = event;
        currentStage = CHOOSING_EVENT_TIME;
        return response.toString();
    }

    private String messageOnChoosingEventTime(String inputLine) {
        int timeLineNumber = Integer.parseInt(inputLine);
        this.localDateTime = new ArrayList<>(event.getAuditoriums().keySet()).get(timeLineNumber - 1);
        var availableSeats = bookingService.getAvailableSeatsForEvent(event, localDateTime);
        if (availableSeats.size() == 0) {
            return messages.getString("view.events.no.seats.available");
        }
        currentStage = CHOOSING_SEATS;
        StringBuilder response = new StringBuilder(messages.getString("view.events.choose.tickets"));
        int i = 0;
        for (Long seat : availableSeats) {
            if (i % 5 == 0) response.append("\n");
            response.append(" | ");
            if (Collections.max(availableSeats) / 10 != 0 && seat / 10 == 0) {
                response.append(" ");
            }
            response.append(seat).append(" (")
                    .append(bookingService.getTicketsPrice(event, localDateTime, user, Collections.singleton(seat)))
                    .append(") | ");
            i++;
        }
        return response.toString();
    }

    private String messageOnChoosingSeats(String inputLine) {
        seats.clear();
        inputLine = inputLine.replaceAll(",", "");
        String[] inputSeats = inputLine.split(" ");
        for (String inputSeat : inputSeats) {
            var seat = Long.valueOf(inputSeat);
            if (!bookingService.getAvailableSeatsForEvent(event, localDateTime).contains(seat)) {
                return messages.getString("view.events.invalid.seat.chosen") + " " + seat;
            }
            seats.add(seat);
        }
        var totalPrice = bookingService.getTicketsPrice(event, localDateTime, user, seats);
        currentStage = CONFIRMATION_TICKET_PURCHASE;
        return String.format(messages.getString("view.events.total.tickets.price"),
                seats.size(),
                totalPrice);
    }

    // :choosing event end


    // admin main menu:

    private String messageOnConfirmPurchase(String inputLine) {
        currentStage = PRESS_ENTER_TO_SHOW_MAIN_MENU;
        if (!inputLine.equalsIgnoreCase("ok")) {
            return messages.getString("buying.tickets.cancel.purchase") + messages.getString("press.enter.to.show.main.menu");
        }
        Set<Ticket> tickets = new HashSet<>();
        seats.forEach(seat -> tickets.add(new Ticket(user, event, localDateTime, seat)));
        bookingService.bookTickets(tickets);
        if (user != null) user.setTickets(new TreeSet<>(tickets));
        return messages.getString("buying.tickets.success") + messages.getString("press.enter.to.show.main.menu");
    }

    private String messageOnMainMenuAdmin(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        switch (choice) {
            case 1:
                return messageOnAddNewEvent();
            case 2:
                return messageOnApplyAuditoriumToTheEvent();
            case 3:
                return messageOnViewPurchasedTickets();
            case 4:
                return messageOnLogout();
            default:
                return messages.getString("menu.main.invalid.choice");
        }

    }

    private String messageOnAddNewEvent() {
        currentStage = ADD_EVENT_ENTERING_EVENT_NAME;
        return messages.getString("add.event.enter.event.name");
    }

    private String messageOnApplyAuditoriumToTheEvent() {
        currentStage = APPLY_AUDITORIUM_CHOOSING_EVENT;
        return messages.getString("apply.auditorium.choose.event.header") + stringifyEventList(eventService.getAll()) + messages.getString("apply.auditorium.choose.event.footer");
    }

    private String messageOnViewPurchasedTickets() {
        currentStage = VIEWING_PURCHASED_TICKETS_CHOOSING_EVENT;
        return messages.getString("view.bought.tickets.choose.event.header") + stringifyEventList(eventService.getAll()) + messages.getString("view.bought.tickets.choose.event.footer");
    }

    // :admin main menu end


    // add new event:

    private String messageWhileAddingNewEvent(String inputLine) {
        switch (currentStage) {
            case ADD_EVENT_ENTERING_EVENT_NAME:
                return messageOnEnteredEventName(inputLine);
            case ADD_EVENT_ENTERING_PRICE:
                return messageOnEnteredEventPrice(inputLine);
            case ADD_EVENT_ENTERING_RATING:
                return messageOnEnteredEventRating(inputLine);
        }
        return null;
    }

    private String messageOnEnteredEventName(String inputLine) {
        event = new Event();
        event.setName(inputLine);
        currentStage = ADD_EVENT_ENTERING_PRICE;
        return messages.getString("add.event.enter.event.price");
    }

    private String messageOnEnteredEventPrice(String inputLine) {
        double price = Double.parseDouble(inputLine);
        event.setBasePrice(price);
        currentStage = ADD_EVENT_ENTERING_RATING;
        StringBuilder response = new StringBuilder(messages.getString("add.event.enter.event.rating"));
        int i = 1;
        for (EventRating value : EventRating.values()) {
            response.append('\n')
                    .append(i)
                    .append(". ")
                    .append(value);
            i++;
        }
        return response.toString();
    }

    private String messageOnEnteredEventRating(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        EventRating[] eventRatings = EventRating.values();
        event.setRating(eventRatings[choice - 1]);
        eventService.save(event);
        currentStage = PRESS_ENTER_TO_SHOW_MAIN_MENU;
        return messages.getString("add.event.success") + '\n' + messages.getString("press.enter.to.show.main.menu");
    }

    // :add new event end


    // apply audience to the event:

    private String messageWhileApplyingAuditoriumToTheEvent(String inputLine) {
        switch (currentStage) {
            case APPLY_AUDITORIUM_CHOOSING_EVENT:
                return messageOnChoosingEventWhileApplyingAuditorium(inputLine);
            case APPLY_AUDITORIUM_ENTERING_DATE:
                return messageOnEnteringDateWhileApplyingAuditorium(inputLine);
            case APPLY_AUDITORIUM_CHOOSING_AUDITORIUM:
                return messageOnChoosingAuditoriumWhileApplyingAuditorium(inputLine);
        }
        return null;
    }

    private String messageOnChoosingEventWhileApplyingAuditorium(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        event = (Event) ((List) eventService.getAll()).get(choice - 1);
        currentStage = APPLY_AUDITORIUM_ENTERING_DATE;
        return messages.getString("apply.auditorium.enter.date");
    }

    private String messageOnEnteringDateWhileApplyingAuditorium(String inputLine) {
        try {
            localDateTime = LocalDateTime.parse(inputLine, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
            currentStage = APPLY_AUDITORIUM_CHOOSING_AUDITORIUM;
            StringBuilder response = new StringBuilder(messages.getString("apply.auditorium.choose.auditorium.header"));
            int i = 1;
            for (Auditorium auditorium : auditoriumService.getAll()) {
                response.append(i++)
                        .append(". ")
                        .append(auditorium.toString())
                        .append('\n');
            }
            return response.append(messages.getString("apply.auditorium.choose.auditorium.footer")).toString();
        } catch (DateTimeParseException e) {
            logger.debug(e);
            return messages.getString("apply.auditorium.invalid.date");
        }
    }

    private String messageOnChoosingAuditoriumWhileApplyingAuditorium(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        Auditorium auditorium = new ArrayList<>(auditoriumService.getAll()).get(choice - 1);
        eventService.applyAuditoriumToEvent(event, auditorium, localDateTime);
        currentStage = PRESS_ENTER_TO_SHOW_MAIN_MENU;
        return messages.getString("apply.auditorium.success") + '\n' + messages.getString("press.enter.to.show.main.menu");
    }

    // :apply audience to the event end


    // view bought tickets:

    private String messageWhileViewingBoughtTickets(String inputLine) {
        if (inputLine.equals("0")) {
            currentStage = MAIN_MENU_ADMIN;
            return messages.getString("menu.main.admin");
        }
        switch (currentStage) {
            case VIEWING_PURCHASED_TICKETS_CHOOSING_EVENT:
                return messageOnChoosingEventWhileViewingBoughtTickets(inputLine);
            case VIEWING_PURCHASED_TICKETS_CHOOSING_TIME:
                return messageOnChoosingEventTimeWhileViewingBoughtTickets(inputLine);
        }
        return null;
    }

    private String messageOnChoosingEventWhileViewingBoughtTickets(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        event = new ArrayList<>(eventService.getAll()).get(choice - 1);
        if (event.getAuditoriums().isEmpty()) {
            return messages.getString("view.bought.tickets.choose.event.no.timings");
        }
        currentStage = VIEWING_PURCHASED_TICKETS_CHOOSING_TIME;
        StringBuilder response = new StringBuilder();
        int i = 1;
        for (var entry : event.getAuditoriums().entrySet()) {
            LocalDateTime dateTime = entry.getKey();
            Auditorium auditorium = entry.getValue();
            Set<Ticket> purchasedTickets = bookingService.getPurchasedTicketsForEvent(event, dateTime);
            long numberOfSeats = auditorium.getNumberOfSeats();
            response.append('\n')
                    .append(i++).append(". ")
                    .append("Date: ")
                    .append(dateTime.format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy")))
                    .append(", Auditorium: ")
                    .append(auditorium.getName())
                    .append(", ").append(purchasedTickets.size()).append('/').append(numberOfSeats).append(" seats purchased");
        }
        return messages.getString("view.bought.tickets.choose.event.time.header") + response.toString() + '\n' + messages.getString("view.bought.tickets.choose.event.time.footer");
    }

    private String messageOnChoosingEventTimeWhileViewingBoughtTickets(String inputLine) {
        int choice = Integer.parseInt(inputLine);
        localDateTime = new ArrayList<>(event.getAuditoriums().keySet()).get(choice - 1);
        currentStage = PRESS_ENTER_TO_SHOW_MAIN_MENU;
        Set<Ticket> purchasedTicketsForEvent = bookingService.getPurchasedTicketsForEvent(event, localDateTime);
        if (purchasedTicketsForEvent.isEmpty()) {
            return messages.getString("view.bought.tickets.not.found") + '\n' + messages.getString("press.enter.to.show.main.menu");
        }
        StringBuilder response = new StringBuilder();
        purchasedTicketsForEvent.forEach(ticket -> {
            response.append("○ ").append(ticket.toString()).append('\n');
        });
        return response.toString() + messages.getString("press.enter.to.show.main.menu");
    }

    // :view bough tickets end


    // misc:

    private String messageOnPressEnterToShowMainMenu() {
        if (user == null) {
            currentStage = MAIN_MENU_USER;
            return messages.getString("menu.main.user");
        } else {
            boolean isUserAdmin = user.getRole() == UserRole.ADMIN;
            currentStage = isUserAdmin ? MAIN_MENU_ADMIN : MAIN_MENU_USER;
            return isUserAdmin ? messages.getString("menu.main.admin") : messages.getString("menu.main.user");
        }
    }

    // :misc end


    // utils:

    private String stringifyEventList(Collection<Event> events) {
        StringBuilder response = new StringBuilder();
        int i = 1;
        for (Event event : events) {
            response.append(i++)
                    .append(". ")
                    .append(event.toString())
                    .append("\n");
        }
        return response.toString();
    }

    // :utils end
}
